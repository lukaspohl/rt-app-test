

/*                                                                  
 * POSIX Real Time Example
 * using a single pthread as RT thread
 */
#define _GNU_SOURCE 
#include <gpiod.h>
#include <limits.h>
#include <pthread.h>
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

struct period_info {
        struct timespec next_period;
        long period_ns;
};

static void inc_period(struct period_info *pinfo) 
{
        pinfo->next_period.tv_nsec += pinfo->period_ns;
 
        while (pinfo->next_period.tv_nsec >= 1000000000) {
                /* timespec nsec overflow */
                pinfo->next_period.tv_sec++;
                pinfo->next_period.tv_nsec -= 1000000000;
        }
}
 
static void periodic_task_init(struct period_info *pinfo)
{
        /* for simplicity, hardcoding a 1ms period */
        pinfo->period_ns = 50000;
 
        clock_gettime(CLOCK_MONOTONIC, &(pinfo->next_period));
}
 
static void do_rt_task()
{
        /* Do RT stuff here. */
}
 
static void wait_rest_of_period(struct period_info *pinfo)
{
        inc_period(pinfo);
 
        /* for simplicity, ignoring possibilities of signal wakes */
        clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &pinfo->next_period, NULL);
}
 
void *simple_cyclic_task(void *data)
{
        struct period_info pinfo;
        int isolCPU = 2;
        cpu_set_t mask;
        pthread_t current_thread = pthread_self();

        /* Push thread to a specific CPU by setting affinity */
        CPU_ZERO(&mask);
        CPU_SET(isolCPU, &mask);
        if (pthread_setaffinity_np(current_thread, sizeof(mask), &mask) == -1) {
               perror("Setting CPU affinity failed\n"); 
               goto end;
        }
        printf("Running on cpu %d\n", sched_getcpu());

        periodic_task_init(&pinfo);
 
	char *chipname = "gpiochip0";
	unsigned int line_num = 24;
	unsigned int val;
	struct gpiod_chip *chip;
	struct gpiod_line *line;
	int i, ret;

	chip = gpiod_chip_open_by_name(chipname);
	if (!chip) {
		perror("Open chip failed\n");
		goto end;
	}
	line = gpiod_chip_get_line(chip, line_num);
	if (!line) {
		perror("Open line failed\n");
		goto end;
	}
	ret = gpiod_line_request_output(line, "Consumer", 0);
	if (ret < 0) {
		perror("Request line as output failed/n");
		gpiod_line_release(line);
		goto end;
	}
	val = 0;

	while (1) {
                //do_rt_task();
		ret = gpiod_line_set_value(line, val);
		if (ret < 0) {
			perror("Set line output failed/n");
			gpiod_line_release(line);
			goto end;
		}
		//printf("Output %u on line #%u\n",val, line_num);
		val = !val;
                wait_rest_of_period(&pinfo);
        }
 end:
        return NULL;
}
 
int main(int argc, char* argv[])
{
        struct sched_param param;
        pthread_attr_t attr;
        pthread_t thread;
        int ret;
 
        /* Lock memory */
        if(mlockall(MCL_CURRENT|MCL_FUTURE) == -1) {
                printf("mlockall failed: %m\n");
                exit(-2);
        }
 
        /* Initialize pthread attributes (default values) */
        ret = pthread_attr_init(&attr);
        if (ret) {
                printf("init pthread attributes failed\n");
                goto out;
        }
 
        /* Set a specific stack size  */
        ret = pthread_attr_setstacksize(&attr, PTHREAD_STACK_MIN);
        if (ret) {
            printf("pthread setstacksize failed\n");
            goto out;
        }
 
        /* Set scheduler policy and priority of pthread */
        ret = pthread_attr_setschedpolicy(&attr, SCHED_FIFO);
        if (ret) {
                printf("pthread setschedpolicy failed\n");
                goto out;
        }
        param.sched_priority = 90;
        ret = pthread_attr_setschedparam(&attr, &param);
        if (ret) {
                printf("pthread setschedparam failed\n");
                goto out;
        }
        /* Use scheduling parameters of attr */
        ret = pthread_attr_setinheritsched(&attr, PTHREAD_EXPLICIT_SCHED);
        if (ret) {
                printf("pthread setinheritsched failed\n");
                goto out;
        }
 
        /* Create a pthread with specified attributes */
        ret = pthread_create(&thread, &attr, simple_cyclic_task, NULL);
        if (ret) {
                printf("create pthread failed\n");
                goto out;
        }
 
        /* Join the thread and wait until it is done */
        ret = pthread_join(thread, NULL);
        if (ret)
                printf("join pthread failed: %m\n");
 
out:
        return ret;
}

